import sys
import random


def apply_noise(line, alpha):
    data = line.strip().split(',')
    pixels = [str(255 - int(pixel)) if random.random() < alpha else pixel for pixel in data[1:]]
    return ','.join(data[:1] + pixels)


def main():
    if len(sys.argv[1:]) != 2:
        return
    input_filename = sys.argv[1]
    alpha = float(sys.argv[2])

    dot_index = input_filename.rfind('.')
    output_filename = input_filename[:dot_index] + '_noisy' + input_filename[dot_index:]
    with open(input_filename) as input_file, open(output_filename, 'w') as output_file:
        for line in input_file:
            output_file.write('%s\n' % apply_noise(line, alpha))


if __name__ == '__main__':
    main()
