import sys
import random


def main():
    if len(sys.argv[1:]) != 2:
        return
    input_filename = sys.argv[1]
    sample_size = int(sys.argv[2])

    dot_index = input_filename.rfind('.')
    output_filename = input_filename[:dot_index] + '_sample' + input_filename[dot_index:]
    with open(input_filename) as input_file:
        data_set = input_file.readlines()
    sample = random.sample(data_set, sample_size)
    with open(output_filename, 'w') as output_fie:
        output_fie.writelines(sample)


if __name__ == '__main__':
    main()
