import sys
import os
import subprocess

PYTHON = 'python'
SAMPLE_GENERATOR = './mnist_sample_generator.py'
NOISE_APPLIER = './mnist_noise_applier.py'
LEARNTREE = './learntree.py'
PREDICT = './predict_error.py'
TRAIN_FILENAME = 'mnist_train.csv'
TEST_FILENAME = 'mnist_test.csv'
TRAIN_NOISY_FILENAME = 'mnist_train_noisy.csv'
TEST_NOISY_FILENAME = 'mnist_test_noisy.csv'
SAMPLE_FILENAME = 'mnist_train_sample.csv'
SAMPLE_NOISY_FILENAME = 'mnist_train_noisy_sample.csv'
TREE_FILENAME = 'tree.p'
TEST_LOG = 'test_log.txt'


def main():
    if len(sys.argv[1:]) not in (5, 6):
        print 'Usage: %s <num_of_tests> <sample_size> [<alpha>] <version> <P> <L>' % sys.argv[0]
        return
    if len(sys.argv[1:]) == 6:
        num = int(sys.argv[1])
        sample_size = sys.argv[2]
        alpha = sys.argv[3]
        version = sys.argv[4]
        p = sys.argv[5]
        l = sys.argv[6]
    else:
        num = int(sys.argv[1])
        sample_size = sys.argv[2]
        alpha = 0
        version = sys.argv[3]
        p = sys.argv[4]
        l = sys.argv[5]

    if float(alpha) > 0:
        print 'Applying noise...'
        subprocess.call([PYTHON, NOISE_APPLIER, TRAIN_FILENAME, alpha])
        subprocess.call([PYTHON, NOISE_APPLIER, TEST_FILENAME, alpha])
        train_filename = TRAIN_NOISY_FILENAME
        test_filename = TEST_NOISY_FILENAME
        sample_filename = SAMPLE_NOISY_FILENAME
        print '...done'
    else:
        train_filename = TRAIN_FILENAME
        test_filename = TEST_FILENAME
        sample_filename = SAMPLE_FILENAME
    with open(TEST_LOG, 'a') as log:
        if float(alpha) > 0:
            log.write('Sample size: %s Noise: %s Version: %s\n' % (sample_size, alpha, version))
        else:
            log.write('Sample size: %s Version: %s\n' % (sample_size, version))
        log.flush()
        os.fsync(log.fileno())
        for i in xrange(num):
            subprocess.call([PYTHON, SAMPLE_GENERATOR, train_filename, sample_size])
            subprocess.call([PYTHON, LEARNTREE, version, p, l, sample_filename, TREE_FILENAME], stdout=log)
            subprocess.call([PYTHON, PREDICT, TREE_FILENAME, test_filename], stdout=log)
            print 'Test %d done' % (i + 1)
        log.flush()
        os.fsync(log.fileno())
        log.write('\n')


if __name__ == '__main__':
    main()
