# Topics in Data Analysis Mini-Project: Learning Tree #

## Overview ##
This project is an implementation of a [decision tree learning](https://en.wikipedia.org/wiki/Decision_tree_learning) algorithm from scratch, which learns to recognize handwritten digits provided by the [MNIST database](http://yann.lecun.com/exdb/mnist/). The algorithm builds a decision tree from a training set of images, with the purpose of using that knowledge to run on a test set of previously unseen images and identify the digits they represent.  
The project was written entirely in Python 3.  
The specifications of the project can be read in `miniproject.pdf`, and my report, including experiments and their results, can be read in `report.pdf`.
