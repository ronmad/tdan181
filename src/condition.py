from functools import partial

DARK = 0
AVG = 1
image_sums = {}


def condition(which, pixels):
    if which == DARK:
        return partial(condition_dark, pixels=pixels)
    return partial(condition_avg, pixels=pixels)


def condition_dark(image, pixels):
    for pix in pixels:
        if not image[pix] > 128:
            return False
    return True


def condition_avg(image, pixels):
    if id(image) in image_sums:
        img_sum = image_sums[id(image)]
    else:
        img_sum = sum(image)
        image_sums[id(image)] = img_sum
    data_avg = img_sum / len(image)
    pixels_avg = sum([image[pix] for pix in pixels]) / len(pixels)
    return pixels_avg > data_avg
