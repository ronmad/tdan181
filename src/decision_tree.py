import constants


class DecisionTree(object):
    item_condition_memo = {}

    def __init__(self):
        self.root = None
        self.leaves = []
        self.size = 0

    def make_leaf(self, value):
        leaf = DecisionLeaf(None, value)
        self.root = leaf
        self.leaves = [leaf]
        return leaf

    def replace_leaf(self, leaf, condition, yes_value, no_value):
        new_node = DecisionNode(leaf.parent, condition, yes_value, no_value)
        self._attach_node(leaf, new_node)
        self.leaves.remove(leaf)
        self.leaves += [new_node.yes, new_node.no]
        self.size += 1
        return new_node

    def _attach_node(self, leaf, node):
        if not node.parent:
            self.root = node
        elif node.parent.yes == leaf:
            node.parent.yes = node
        elif node.parent.no == leaf:
            node.parent.no = node

    def decide(self, image):
        curr = self.root
        while not isinstance(curr, DecisionLeaf):
            if curr(image):
                curr = curr.yes
            else:
                curr = curr.no
        return curr.value

    @staticmethod
    def dataset_filter_condition(condition, data_set):

        def passes_condition(item):
            image = item[constants.IMAGE]
            if (id(item), condition) in DecisionTree.item_condition_memo:
                res = DecisionTree.item_condition_memo[id(item), condition]
            else:
                res = condition(image)
                DecisionTree.item_condition_memo[id(item), condition] = res
            return res
        return [item for item in data_set if passes_condition(item)]

    @staticmethod
    def dataset_filter_node(node, data_set):
        if not (node and node.parent):
            return data_set

        def reaches_node(item):
            image = item[constants.IMAGE]
            curr = node
            res = True
            while curr.parent and res:
                if (id(item), curr.parent.condition) in DecisionTree.item_condition_memo:
                    res = DecisionTree.item_condition_memo[id(item), curr.parent.condition]
                else:
                    res = curr.parent(image)
                    DecisionTree.item_condition_memo[id(item), curr.parent.condition] = res
                if curr.parent.yes == curr:
                    pass
                elif curr.parent.no == curr:
                    res = not res
                curr = curr.parent
            return res
        return [item for item in data_set if reaches_node(item)]


class DecisionNode(object):
    def __init__(self, parent, condition=None, yes_value=None, no_value=None):
        self.parent = parent
        self.condition = condition
        if yes_value is not None:
            self.yes = DecisionLeaf(self, yes_value)
        if no_value is not None:
            self.no = DecisionLeaf(self, no_value)

    def __call__(self, image):
        return self.condition(image)


class DecisionLeaf(DecisionNode):
    def __init__(self, parent, value):
        super(DecisionLeaf, self).__init__(parent)
        self.value = value
