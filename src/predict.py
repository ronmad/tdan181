import sys
import pickle


def read_data_set(data_file):
    test_set = [get_pixel_data(line) for line in data_file]
    data_file.close()
    return test_set


def get_pixel_data(line):
    data = line.strip().split(',')
    return map(int, data[1:])


def predict(tree, data_set):
    for image in data_set:
        print tree.decide(image)


def main():
    if len(sys.argv[1:]) != 2:
        sys.exit("Invalid arguments")
    try:
        tree_file = open(sys.argv[1], 'rb')
        testset_file = open(sys.argv[2])
        decision_tree = pickle.load(tree_file)
    except IOError as e:
        sys.exit('Error opening file "%s": %s' % (e.filename, e.strerror))
    except pickle.UnpicklingError:
        sys.exit("Problem reading the tree")

    test_set = read_data_set(testset_file)
    predict(decision_tree, test_set)
    tree_file.close()


if __name__ == '__main__':
    main()
