import random
import constants
from math import log
from collections import defaultdict
from decision_tree import DecisionTree


class Memoize(object):
    def __init__(self, f):
        self.f = f
        self.memo = {}


class IGMemoize(Memoize):
    def __init__(self, calculation_func):
        super(IGMemoize, self).__init__(calculation_func)

    def __call__(self, leaf, data_set, condition_set):
        if leaf in self.memo:
            ret = self.memo[leaf]
        else:
            ret = self.f(leaf, data_set, condition_set)
            self.memo[leaf] = ret
        return ret


def get_label_freqs(labels):
    freqs = defaultdict(int)
    for label in labels:
        freqs[label] += 1
    return freqs


def most_common_label(data_set):
    return freqs_most_common_label(get_label_freqs((label for label, image in data_set)))


def freqs_most_common_label(label_freqs):
    if not label_freqs:
        return random.sample(constants.LABEL_SET, 1)[0]
    return max(label_freqs, key=label_freqs.get)


def calculate_leaf_entropy(label_freqs):
    if not label_freqs:
        return 0
    h_l = 0.0
    n_l = float(sum(label_freqs.values()))
    if n_l == 0:
        return h_l
    for n_i_l in label_freqs.values():
        if n_i_l == 0:
            continue
        h_l += n_i_l / n_l * log(n_l / n_i_l, 2)
    return h_l


def calculate_weighted_entropy(labels_node, labels_yes, labels_no):
    n_l = sum(labels_node.values())
    n_l_a = sum(labels_yes.values())
    n_l_b = sum(labels_no.values())
    h_l_a = calculate_leaf_entropy(labels_yes)
    h_l_b = calculate_leaf_entropy(labels_no)
    if n_l == 0:
        return 0
    return (n_l_a * h_l_a + n_l_b * h_l_b) / float(n_l)


@IGMemoize
def calculate_information_gain(leaf, data_set, condition_set):
    max_i_g_leaf = 0
    best_cond_leaf = random.choice(condition_set)
    yes_value = random.sample(constants.LABEL_SET, 1)[0]
    no_value = random.sample(constants.LABEL_SET, 1)[0]
    filtered_data_set = DecisionTree.dataset_filter_node(leaf, data_set)
    label_freqs = get_label_freqs((label for label, image in filtered_data_set))
    n_l = sum(label_freqs.values())
    h_l = calculate_leaf_entropy(label_freqs)
    for cond in condition_set:
        data_set_yes = DecisionTree.dataset_filter_condition(cond, filtered_data_set)
        freqs_yes = get_label_freqs((label for label, image in data_set_yes))
        freqs_no = {l: f - freqs_yes[l] if l in freqs_yes else f for l, f in label_freqs.iteritems()}
        h_x = calculate_weighted_entropy(label_freqs, freqs_yes, freqs_no)
        i_g = n_l * (h_l - h_x)
        if i_g > max_i_g_leaf:
            max_i_g_leaf = i_g
            best_cond_leaf = cond
            yes_value = freqs_most_common_label(freqs_yes)
            no_value = freqs_most_common_label(freqs_no)
    return max_i_g_leaf, best_cond_leaf, yes_value, no_value
