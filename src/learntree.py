import sys
import pickle
import random
import constants
import funcs
import condition
from math import sqrt
from copy import deepcopy
from decision_tree import DecisionTree

pixel_count = 0     # Assuming square images


def parse_args():
    if len(sys.argv[1:]) < 5:
        sys.exit('Invalid arguments')
    version = int(sys.argv[1])
    validation_percent = int(sys.argv[2])
    max_tree_size = int(sys.argv[3])

    if version not in (1, 2):
        sys.exit('Invalid version number')
    if validation_percent not in xrange(100):
        sys.exit('Invalid validation percentage')
    try:
        trainingset_file = open(sys.argv[4])
        output_file = open(sys.argv[5], 'wb')
    except IOError as e:
        sys.exit('Error opening file "%s": %s' % (e.filename, e.strerror))

    return version, validation_percent, max_tree_size, trainingset_file, output_file


def read_data_set(data_file, validation_percent):
    global pixel_count
    training_set = [get_data(line) for line in data_file]
    pixel_count = len(training_set[0][constants.IMAGE])
    validation_size = int(validation_percent / 100.0 * len(training_set))
    validation_set = [training_set.pop(random.randrange(len(training_set))) for _ in xrange(validation_size)]
    data_file.close()
    return training_set, validation_set


def get_data(line):
    data = line.strip().split(',')
    label = data[0]
    image = map(int, data[1:])
    constants.LABEL_SET.add(label)
    return label, image


def run_validation(tree, data_set):
    miss_count = 0
    for label, image in data_set:
        if tree.decide(image) != label:
            miss_count += 1
    return float(miss_count) / len(data_set) * 100


def make_condition_set_1():
    return [condition.condition(condition.DARK, [pix]) for pix in xrange(pixel_count)]


def make_condition_set_2():
    dim = int(sqrt(pixel_count))
    rows = [[j for j in xrange(dim * i, dim * (i+1))] for i in xrange(dim)]
    cols = [[r[i] for r in rows] for i in xrange(dim)]
    diagonals = [r[i] for r, i in zip(rows, xrange(dim))], [r[i] for r, i in zip(rows, xrange(dim-1, -1, -1))]
    circles = ([rows[x][y+1]] + rows[x+1][y:y+3] + [rows[x+2][y+1]] for x in xrange(dim-2) for y in xrange(dim-2))
    condition_set = [condition.condition(condition.AVG, row_col) for row_col in rows + cols] \
        + [condition.condition(condition.AVG, diag) for diag in diagonals] \
        + [condition.condition(condition.AVG, c) for c in circles]
    return condition_set


def build_and_choose_tree(version, max_size, training_set, validation_set):
    acc_tree = DecisionTree()
    acc_tree.make_leaf(funcs.most_common_label(training_set))
    best_tree = acc_tree
    min_error = 100
    tree_size = 1
    condition_set = make_condition_set_1() if version == 1 else make_condition_set_2()
    while tree_size <= 2 ** max_size:
        acc_tree = build_tree(tree_size, training_set, condition_set, acc_tree)
        error = run_validation(acc_tree, validation_set)
        if error < min_error:
            min_error = error
            best_tree = deepcopy(acc_tree)
        tree_size *= 2
    error = run_validation(best_tree, training_set)
    return best_tree, int(error)


def build_tree(size, data_set, condition_set, decision_tree):
    while decision_tree.size < size:
        choose_and_substitute_leaf(decision_tree, data_set, condition_set)
    return decision_tree


def choose_and_substitute_leaf(tree, data_set, condition_set):
    max_i_g = 0
    chosen_leaf = random.choice(tree.leaves)
    chosen_condition = random.choice(condition_set)
    chosen_yes_value = random.sample(constants.LABEL_SET, 1)[0]
    chosen_no_value = random.sample(constants.LABEL_SET, 1)[0]
    for leaf in tree.leaves:
        max_i_g_leaf, best_cond_leaf, yes_value, no_value = \
            funcs.calculate_information_gain(leaf, data_set, condition_set)
        if max_i_g_leaf > max_i_g:
            max_i_g = max_i_g_leaf
            chosen_leaf = leaf
            chosen_condition = best_cond_leaf
            chosen_yes_value = yes_value
            chosen_no_value = no_value
    tree.replace_leaf(chosen_leaf, chosen_condition, chosen_yes_value, chosen_no_value)


def main():
    version, validation_percent, max_tree_size, trainingset_file, output_file = parse_args()

    training_set, validation_set = read_data_set(trainingset_file, validation_percent)
    decision_tree, tree_error = build_and_choose_tree(version, max_tree_size, training_set, validation_set)

    print 'num: %d' % (len(training_set) + len(validation_set))
    print 'error: %d' % tree_error
    print 'size: %d' % decision_tree.size

    del DecisionTree.item_condition_memo        #
    del decision_tree.leaves                    # No longer needed. Takes up space
    pickle.dump(decision_tree, output_file, pickle.HIGHEST_PROTOCOL)
    output_file.close()


if __name__ == '__main__':
    main()
